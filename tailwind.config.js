module.exports = {
  content: [
    "./src/**/*.njk",
    "./src/**/*.js",
    "./src/**/*.svg",
    "./src/**/*.md",
  ],
  plugins: [require("@tailwindcss/forms"), require("@tailwindcss/typography")],
  theme: {
    extend: {
      fontFamily: {
        inter: '"Inter", sans-serif;',
      },
      typography: {
        DEFAULT: {
          css: {
            "code::before": { content: "" },
            "code::after": { content: "" },
          },
        },
      },
    },
  },
};
