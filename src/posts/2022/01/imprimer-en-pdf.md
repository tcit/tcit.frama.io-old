---
title: Imprimer en PDF
date: 2022-01-17T09:20:00.00Z
tags:
  - Fedora
  - Linux
  - Impression
  - PDF
layout: post
---
Quand je veux imprimer un fichier PDF sur une imprimante jamais configurée, ça prend souvent des heures sans raison. 

<!--more-->

Je constate alors ce message dans les logs :

```
Use "pdftops-renderer" option (see README file) to use Ghostscript or MuPDF for the PDF -> PostScript conversion
```

Globalement il me semble que c'est la conversion de PDF en un truc compréhensible par l'imprimante (Ghostscript, PostScript) qui rate.

Sur le web, il est proposé d'utiliser `pdftops` comme outil de rendu (qui semble utiliser `Poppler`), mais ce qui marche le mieux pour moi c'est directement d'utiliser `Ghostscript`.

```bash
lpadmin -p mon_imprimante -o pdftops-renderer-default=gs
```

Où `mon_imprimante` est le nom de l'imprimante, que vous pouvez trouver avec `lpstat -p`.

L'impression se fait alors quasi-instantanément.