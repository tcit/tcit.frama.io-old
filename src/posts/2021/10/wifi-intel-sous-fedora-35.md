---
title: Faire fonctionner le wifi Intel sous Fedora 35
date: 2021-10-04T09:20:00.00Z
tags:
  - Fedora
  - wifi
  - Intel
layout: post
---
Si vous avez migré vers la version 35 de Fedora et que vous utilisez une carte Wi-Fi Intel, cela peut vous aider.

<!--more-->

Tout d'abord, voyons si vous êtes concerné :
```
lspci | grep "Network controller"
02:00.0 Network controller: Intel Corporation Wi-Fi 6 AX200 (rev 1a)
```

Si la référence de votre carte correspond à `AX2xx`, vous êtes probablement concerné.

En Octobre 2021 le microcode pour cette carte [a été migré](https://bodhi.fedoraproject.org/updates/FEDORA-2021-e353a56911) dans un paquet séparé du fameux `linux-firmware`.

Il faut alors installer `iwlax2xx-firmware` pour faire refonctionner le Wi-Fi. Un redémarrage et c'est reparti.
