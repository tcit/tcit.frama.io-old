---
layout: static
title: "À propos"
path: about
---
Je m'appelle Thomas Citharel, et je développe des outils web au service d'un « autre » numérique. Je télé-travaille depuis Nantes, France.

Je suis actuellement salarié chez [Framasoft](https://framasoft.org/), une association loi 1901 qui œuvre pour un «&nbsp;monde numérique émancipateur&nbsp;».
Parmi [une multitude de projets](https://framasoft.org/full), l'association propose notamment des [services libres et éthiques ainsi que des outils conviviaux](https://degooglisons-internet.org), et finance aussi le développement de logiciels comme [PeerTube](https://joinpeertube.org) et [Mobilizon](/mobilizon).


Vous pouvez avoir un aperçu de mes projets professionnels et personnels sur la page «&nbsp;[Projets](/projects)&nbsp;».

Outre cela, j'aime bien passer mon temps libre à contribuer à des bases de connaissances libres comme [Wikipédia](https://fr.wikipedia.org), [Wikidata](https://www.wikidata.org) et[OpenStreetMap](https://www.openstreetmap.org). Je fais aussi quelques photos.