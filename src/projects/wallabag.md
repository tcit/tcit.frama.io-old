---
title: wallabag
start_date: "2014"
end_date: "2022"
excerpt: wallabag est une application libre de lecture différée.
link: https://wallabag.org/
tags:
  - PHP
  - Symfony
  - wallabag
  - Read-It-Later
thumbnail: wallabag_logo.png
layout: static
path: "/wallabag"
order: 3
---
J'ai commencé à contribuer au projet **wallabag**¹ en 2014, alors qu'il se nommait encore « Poche ». C'est ici que j'ai effectué mes premières contributions à des projets libres.

Comme souvent, j'ai commencé par être utilisateur du projet, puis un point particulier m'embêtait au point d'aller farfouiller dans le code pour comprendre comment corriger un souci ou apporter une fonctionnalité. De fil en aiguille j'ai passé beaucoup de temps dessus, notamment sur des fonctionnalités comme l'export epub, la recherche ou bien les annotations. J'ai aussi beaucoup travaillé sur le *frontend* ainsi que l'application Android.

Je me suis au fil du temps intégré dans l'équipe principale de développement, mais je n'ai hélas guère plus le temps de contribuer beaucoup, je me force juste à sortir des versions de l'application Android de temps en temps.

¹ Notez l'absence de capitale au début du nom.